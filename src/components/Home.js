import React, { useEffect, useState } from "react";
import "./home.css";
import ReactCardFlip from "react-card-flip";
import axios from "axios";
import Navb from "./Navbar";
import { Button } from "react-bootstrap";

const Home = () => {
  const [isFlipped, setIsFlipped] = useState(false);
  const [chars, setChars] = useState([]);
  const [page, setPage] = useState(1);
  const [search, setSearch] = useState("");
  const [chardId, setCardId] = useState("");

  useEffect(() => {
    axios
      .get(
        `https://rickandmortyapi.com/api/character/?page=${page}&name=${search}`
      )
      .then((res) => {
        setChars(res.data.results);
      })
      .catch((err) => {
        alert(err.message);
      });
  }, [page, search]);
  const handleClick = (charid) => {
    setIsFlipped(!isFlipped);
    setCardId(charid);

    setPage();
  };

  // saving character  *********************************************/
  const saveHandle = (e) => {
    const savedChar = JSON.parse(localStorage.getItem("charData") || "[]");

    const clickSave = chars.filter((item) => e === item.id);
    const charName = clickSave.map((item) => item.name);
    const dup = charName.toString();
    const savedCharName = savedChar.map((item) => item.name);
    const dupChar = savedCharName.find((item) => item === dup);
    if (dupChar !== undefined) {
      alert("Already Saved. Please Save Another Item");
    }

    if (savedChar.length === 0) {
      clickSave.forEach((item) => {
        const save2 = [...savedChar, item];
        localStorage.setItem("charData", JSON.stringify(save2));
      });
    } else {
      clickSave.forEach((item) => {
        const obj = new Object(item);
        savedChar.push(obj);
        const uniqChars = [
          ...savedChar
            .reduce((map, obj) => map.set(obj.id, obj), new Map())
            .values(),
        ];
        localStorage.setItem("charData", JSON.stringify(uniqChars));
      });
    }
  };

  //End saving Character
  //***************************************************************** */

  return (
    <div className="main-container">
      <div>
        <Navb searchItem={setSearch} search={search} />
        <div className="flipcard">
          {chars.map((char, key) => {
            return (
              <div key={char.id}>
                <div key={char.id} className="card-item">
                  <div className="cards">
                    <ReactCardFlip
                      isFlipped={chardId === char.id && isFlipped}
                      flipSpeedFrontToBack={1.0}
                      flipSpeedBackToFront={1.0}
                      flipDirection="vertical"
                      key={char.id}
                    >
                      <div
                        onClick={() => handleClick(char.id)}
                        className="handleSize"
                        key={char.id}
                      >
                        <img src={char.image} alt="char logo" />
                      </div>
                      <div
                        onClick={() => handleClick(char.id)}
                        className="handleSize"
                        key={char.id}
                      >
                        <div className="char-info">
                          <p>Name : {char.name}</p>
                          <p>Status : {char.status}</p>
                          <p>Gender : {char.gender}</p>
                        </div>
                      </div>
                    </ReactCardFlip>
                  </div>
                  <Button
                    variant="secondary bit-position"
                    onClick={() => saveHandle(char.id)}
                    className="save-btn"
                  >
                    Save
                  </Button>{" "}
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Home;
