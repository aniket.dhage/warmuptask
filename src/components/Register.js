import React, { useState } from "react";
import Login from "./Login";
import "./form.css";
const Register = () => {
  const [fName, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [flag, setFlag] = useState(false);
  const [login, setLogin] = useState(true);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!fName || !email || !password) {
      setFlag(true);
    } else {
      setFlag(false);

      var formUsers = JSON.parse(localStorage.getItem("formUsers") || "[]");
      var formUser = {
        fName: fName,
        email: email,
        password: password,
      };
      // localStorage.setItem("formUsers", JSON.stringify(rawUser));
      const users = localStorage.getItem("formUsers");
      // console.log(users);
      let allentry = JSON.parse(users);
      if (allentry === null) {
        formUsers.push(formUser);
        localStorage.setItem("formUsers", JSON.stringify(formUsers));
      } else {
        for (let i = 0; i < allentry.length; i++) {
          let entry = allentry[i];

          let nemail = entry.email;
          if (nemail === email) {
            alert("already registered. please log in");
          } else {
            formUsers.push(formUser);
            localStorage.setItem("formUsers", JSON.stringify(formUsers));
            setLogin(login);
          }
        }
      }

      setLogin(!login);
    }
  };
  const handleClick = () => {
    setLogin(!login);
  };

  return (
    <div>
      {login ? (
        <div className="center">
          <h1>Register</h1>
          <form onSubmit={handleSubmit}>
            <div className="txt_field">
              <input
                className="input"
                type="text"
                name="name"
                value={fName}
                onChange={(e) => {
                  setName(e.target.value);
                }}
                placeholder="Enter your name"
              />
              {/* <label className="label">Name : </label> */}
            </div>
            <div className="txt_field">
              <input
                className="input"
                type="email"
                name="name"
                value={email}
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
                placeholder="Enter your Email"
              />
              {/* <label className="label">Email : </label> */}
            </div>
            <div className="txt_field">
              <input
                className="input"
                type="text"
                name="name"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                placeholder="Enter Password"
              />
              {/* <label className="label">Password : </label> */}
            </div>
            <button type="submit" className="btn btn-dark btn-lg sub-btn">
              Register
            </button>
            <br />

            <p onClick={handleClick}>Already user</p>
            {flag && (
              <div className="alert alert-danger" role="alert">
                I got it you are in hurry! But every Field is important!
              </div>
            )}
          </form>
        </div>
      ) : (
        <Login />
      )}
    </div>
  );
};

export default Register;
