import React, { useState } from "react";
import Home from "./Home";
import "./form.css";

const Login = () => {
  const [home, setHome] = useState(true);
  const [emaillog, setEmaillog] = useState("");
  const [passwordlog, setPasswordlog] = useState("");
  const [flag, setFlag] = useState(false);
  // const [user, setUser] = useState([]);

  let users = localStorage.getItem("formUsers");
  // console.log(users);
  // console.log(users.email);
  const handleLogin = (e) => {
    e.preventDefault();
    // saved.forEach((element) => {
    //   setSave([...save, element]);
    //   localStorage.setItem("charData", JSON.stringify(save));
    // });
    let allentry = JSON.parse(users);
    if (allentry === null) {
      alert("You Are Not Registered. Please Register First");
    } else {
      for (let i = 0; i < allentry.length; i++) {
        let entry = allentry[i];
        let email = entry.email;
        let pass = entry.password;
        if (!emaillog || !passwordlog) {
          setFlag(true);
        } else if (passwordlog !== pass || emaillog !== email) {
          setFlag(true);
        } else {
          setHome(!home);
          setFlag(false);
        }
      }
    }
  };
  return (
    <div>
      {home ? (
        <div className="center">
          <h1>Login</h1>
          <form onSubmit={handleLogin}>
            <div className="txt_field">
              <input
                className="input"
                type="email"
                name="email"
                placeholder="Enter your Email"
                onChange={(e) => setEmaillog(e.target.value)}
              />
            </div>
            <div className="txt_field">
              <input
                className="input"
                type="text"
                name="password"
                placeholder="Enter Password"
                onChange={(e) => setPasswordlog(e.target.value)}
              />
            </div>
            <button type="submit" className="btn btn-dark btn-lg sub-btn">
              Login
            </button>
            <p></p>
            {/* <p onClick={handleClick}>Already user</p> */}
            {flag && (
              <div className="alert alert-danger" role="alert">
                Please fill Correct Info
              </div>
            )}
          </form>
        </div>
      ) : (
        <Home />
      )}
    </div>
  );
};

export default Login;
