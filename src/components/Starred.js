import React, { useState } from "react";
import ReactCardFlip from "react-card-flip";
// import Navb from "./Navbar";
import { Button } from "react-bootstrap";
import "./home.css";
import Navb2 from "./navbarSave";

const Starred = () => {
  const savedChar = JSON.parse(localStorage.getItem("charData") || "[]");
  // console.log(savedChar);
  const [newSaved, setNewSaved] = useState(savedChar);
  const [chardId, setCardId] = useState("");
  const [isFlipped, setIsFlipped] = useState(false);
  if (savedChar === null) {
    alert("Your Saved Page Is Empty. Please Save Cards");
  }
  const handleClick = (charid) => {
    setIsFlipped(!isFlipped);
    setCardId(charid);
  };

  const deleteHandler = (charid) => {
    setNewSaved(newSaved.filter((item) => item.id !== charid));
    // localStorage.setItem("charData", JSON.stringify(newSaved) || "[]");
  };
  localStorage.setItem("charData", JSON.stringify(newSaved));
  return (
    <div className="main-container">
      <div>
        <Navb2 />
        <h1 className="empty-card-page">
          {savedChar.length === 0
            ? "Your Saved Cards Is Empty"
            : "Your Saved Items"}
        </h1>
        <div className="flipcard">
          {newSaved.map((char, key) => {
            return (
              <div key={key} className="card-item">
                <div className="cards">
                  <ReactCardFlip
                    isFlipped={chardId === char.id && isFlipped}
                    flipSpeedFrontToBack={1.0}
                    flipSpeedBackToFront={1.0}
                    flipDirection="vertical"
                    className="flipcard"
                  >
                    <div
                      onClick={() => handleClick(char.id)}
                      className="handleSize"
                    >
                      <img src={char.image} alt="char logo" />
                    </div>
                    <div
                      onClick={() => handleClick(char.id)}
                      className="handleSize"
                    >
                      <div className="char-info">
                        <p>Name : {char.name}</p>
                        <p>Status : {char.status}</p>
                        <p>Gender : {char.gender}</p>
                      </div>
                    </div>
                  </ReactCardFlip>
                </div>
                <Button
                  variant="secondary bit-position"
                  onClick={() => deleteHandler(char.id)}
                  className="save-btn"
                >
                  Delete
                </Button>{" "}
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Starred;
