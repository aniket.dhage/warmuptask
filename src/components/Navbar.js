import React from "react";
import { Link } from "react-router-dom";
import "./nav.css";
import { Button, Form, Nav, Navbar, Container } from "react-bootstrap";
const Navb = ({ searchItem, search }) => {
  return (
    <Navbar bg="light" expand="lg">
      <Container fluid>
        <Navbar.Brand>Warm Up Task</Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-0"
            style={{ maxHeight: "100px" }}
            navbarScroll
          >
            <Nav.Link as={Link} to="/home">
              Home
            </Nav.Link>
            <Nav.Link as={Link} to="/saved">
              Saved
            </Nav.Link>

            <Nav.Link as={Link} to="/login">
              SignOut
            </Nav.Link>
          </Nav>
          <Form className="d-flex">
            <input
              // className="form-control mr-sm-2 item-search search-field"
              type="search"
              className="me-2"
              placeholder="Search"
              value={search}
              onChange={(e) => searchItem(e.target.value)}
            />
            <Button variant="outline-success">Search</Button>
          </Form>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default Navb;
