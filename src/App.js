import "./App.css";

import "../node_modules/bootstrap/dist/css/bootstrap.css";
import Home from "./components/Home";
import Starred from "./components/Starred";
import Register from "./components/Register";
import { Route, Switch } from "react-router-dom";
import Login from "./components/Login";

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/" component={Register} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/home" component={Home} />
        <Route exact path="/saved" component={Starred} />
      </Switch>
      {/*   */}
    </div>
  );
}

export default App;
